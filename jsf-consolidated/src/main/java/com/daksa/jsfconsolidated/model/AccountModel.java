/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.jsfconsolidated.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 *
 * @author Ilham
 */


public class AccountModel {

    private String id;
    private String cifCode;
    private String accountNo;
    private String accountTipe;
    private String kodeMataUang;
    private String accountStatus;
    private BigDecimal saldoGLAkhir;
    @Temporal(TemporalType.DATE)
    private Date tglReport;
    @Temporal(TemporalType.DATE)
    private Date createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCifCode() {
        return cifCode;
    }

    public void setCifCode(String cifCode) {
        this.cifCode = cifCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountTipe() {
        return accountTipe;
    }

    public void setAccountTipe(String accountTipe) {
        this.accountTipe = accountTipe;
    }

    public String getKodeMataUang() {
        return kodeMataUang;
    }

    public void setKodeMataUang(String kodeMataUang) {
        this.kodeMataUang = kodeMataUang;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public BigDecimal getSaldoGLAkhir() {
        return saldoGLAkhir;
    }

    public void setSaldoGLAkhir(BigDecimal saldoGLAkhir) {
        this.saldoGLAkhir = saldoGLAkhir;
    }

    public Date getTglReport() {
        return tglReport;
    }

    public void setTglReport(Date tglReport) {
        this.tglReport = tglReport;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
