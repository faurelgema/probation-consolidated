/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.jsfconsolidated.model;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Ilham
 */
@Named("treeBasicView")
@ViewScoped
public class BasicView implements Serializable {

    private TreeNode root;

    @PostConstruct
    public void init() {
        root = new DefaultTreeNode("Files", null);
        TreeNode node0 = new DefaultTreeNode("Consolidated Management", root);


        node0.getChildren().add(new DefaultTreeNode("Fetch Data"));
        node0.getChildren().add(new DefaultTreeNode("Consolidated Statement"));
        node0.getChildren().add(new DefaultTreeNode("Report PDF"));
       
    }

    public TreeNode getRoot() {
        return root;
    }
}
