/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.jsfconsolidated.controller;

/**
 *
 * @author Ilham
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class FetchController implements Serializable{

    public void file1() {
        try {
            BufferedReader buf = new BufferedReader(new FileReader("D:/kerja/fetchfile/y47acpf"));
            ArrayList<String> words = new ArrayList<>();
            String lineJustFetched = null;
            String[] wordsArray;

            while (true) {
                lineJustFetched = buf.readLine();
                if (lineJustFetched == null) {
                    break;
                } else {
                    wordsArray = lineJustFetched.split("\t");
                    for (String each : wordsArray) {
                        if (!"".equals(each)) {
                            words.add(each);
                        }
                    }
                }
            }

            for (String each : words) {
                System.out.println(each);
            }

            buf.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
