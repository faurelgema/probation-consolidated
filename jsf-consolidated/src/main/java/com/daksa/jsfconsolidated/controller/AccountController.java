/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.jsfconsolidated.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Muh Kanda Wibawa Putra (Melod)
 */
@Table(name = "Account")
@Entity
public class AccountController implements Serializable {
    @Id
    @Column(name = "id", length = 32)
    private String id;
    @Column(name = "cif_code", length = 6)
    private String cifCode;
    @Column(name = "account_no", length = 13)
    private String accountNo;
    @Column(name = "account_tipe", length = 2)
    private String accountTipe;
    @Column(name = "kode_mata_uang", length = 3)
    private String kodeMataUang;
    @Column(name = "account_status", length = 1)
    private String accountStatus;
    @Column(name = "saldo_gl_akhir", scale = 2, precision = 32)
    private BigDecimal saldoGLAkhir;
    @Column(name = "tgl_report")
    @Temporal(TemporalType.DATE)
    private Date tglReport;
    @Temporal(TemporalType.DATE)
    @Column(name = "created_date", nullable = false)
    private Date createdDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCifCode() {
        return cifCode;
    }

    public void setCifCode(String cifCode) {
        this.cifCode = cifCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountTipe() {
        return accountTipe;
    }

    public void setAccountTipe(String accountTipe) {
        this.accountTipe = accountTipe;
    }

    public String getKodeMataUang() {
        return kodeMataUang;
    }

    public void setKodeMataUang(String kodeMataUang) {
        this.kodeMataUang = kodeMataUang;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public BigDecimal getSaldoGLAkhir() {
        return saldoGLAkhir;
    }

    public void setSaldoGLAkhir(BigDecimal saldoGLAkhir) {
        this.saldoGLAkhir = saldoGLAkhir;
    }

    public Date getTglReport() {
        return tglReport;
    }

    public void setTglReport(Date tglReport) {
        this.tglReport = tglReport;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    
}
